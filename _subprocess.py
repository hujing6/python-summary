# -*- coding: utf-8 -*-
"""
@time: 2023/3/29 0:48
"""
import subprocess


if __name__ == '__main__':
    cmd = ["cd"]
    status = subprocess.call(cmd, shell=True) # os.system(cmd)
    # 0
    completeProcess = subprocess.run(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    # CompletedProcess(args=['cd'], returncode=0, stdout=b'E:\\ProgramWorkSpace\\PythonWS\\algorithm\r\n')
    stdout = subprocess.getoutput("cd") # os.popen(cmd).read()
    # 'E:\\ProgramWorkSpace\\PythonWS\\algorithm'
    stdout_status = subprocess.getstatusoutput("cd") #  返回一个元组(命令执行状态, 命令执行结果输出)
    # <class 'tuple'>: (0, 'E:\\ProgramWorkSpace\\PythonWS\\algorithm')
    check_status = subprocess.check_call(cmd, shell=True) # 执行指定的命令，如果执行成功则返回状态码，否则抛出异常
    # 0
    subprocess.check_output(cmd, shell=True) # 如果执行状态码为0则返回命令执行结果，否则抛出异常。
    # b'E:\\ProgramWorkSpace\\PythonWS\\algorithm\r\n'

    pp = subprocess.Popen(["python"], shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    pp.stdin.write("print(1)\n".encode("utf-8"))
    pp.stdin.write("print(2)\n".encode("utf-8"))
    pp.stdin.write("print(3)\n".encode("utf-8"))
    out, err = pp.communicate()
    print(out.decode("utf-8"))
    # communicate 后 read 和 write 都会被关闭
    pp.kill()
    # pp.terminate() # 结束子进程

    pp2 = subprocess.Popen(["powershell"], shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    pp2.stdin.write("cd ../\n".encode("utf-8"))
    pp2.stdin.write("cd".encode("utf-8"))
    out, err = pp2.communicate()
    print(out.decode("utf-8"))
    # communicate 后 read 和 write 都会被关闭
    pp2.kill()
    # pp2.terminate() # 结束子进程