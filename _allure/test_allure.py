# -*- coding:utf-8 -*-
import os

import allure
import pytest


@pytest.fixture(scope="session", autouse=True)
def login_fixture():
    print("前置登录")


@allure.step("步骤1")
def step1():
    print("用户名存在")


@allure.step("步骤2")
def step2():
    print("用户名不存在")


@allure.step("步骤3")
def step3():
    print("密码错误")


@allure.step("步骤4")
def step4():
    print("密码正确")


@allure.epic("用户")
@allure.feature("登录")
class TestLogin(object):

    @allure.story("登录成功")
    @allure.title("title-登录成功")
    @allure.description("description-登录成功")
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.link(url="http://test", name="链接")
    @allure.testcase(url="http://test", name="测试用例")
    @allure.issue(url="http://test", name="缺陷")
    def test_login_success(self):
        step1()
        step4()
        print("登录成功")

    @allure.story("登录失败")
    @allure.title("用户不存在")
    @allure.description("用户名错误")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.link(url="http://test", name="链接")
    @allure.testcase(url="http://test", name="测试用例")
    @allure.issue(url="http://test", name="缺陷")
    def test_login_error1(self):
        step2()
        step4()
        print("登录失败")

    @allure.story("登录失败")
    @allure.title("用户存在，密码错误")
    @allure.description("密码错误")
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.link(url="http://test", name="链接")
    @allure.testcase(url="http://test", name="测试用例")
    @allure.issue(url="http://test", name="缺陷")
    def test_login_error2(self):
        step1()
        step3()
        print("登录失败")


@allure.epic("支付")
@allure.feature("余额")
class TestPay(object):

    def pay_password_error(self):
        print("支付密码错误")
        return False

    def pay_password_correct(self):
        print("支付密码正确")
        return True

    def not_sufficient_funds(self):
        print("余额不足")
        return False

    def sufficient_funds(self):
        print("余额充足")
        return True

    @allure.story("支付成功")
    @allure.description("余额支付成功")
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.link(url="http://test", name="链接")
    @allure.testcase(url="http://test", name="测试用例")
    @allure.issue(url="http://test", name="缺陷")
    def test_pay_success(self):
        assert self.pay_password_correct() and self.sufficient_funds()

    @allure.story("支付失败成功")
    @allure.description("余额不足")
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.link(url="http://test", name="链接")
    @allure.testcase(url="http://test", name="测试用例")
    @allure.issue(url="http://test", name="缺陷")
    def test_pay_error1(self):
        assert self.pay_password_correct() and not self.not_sufficient_funds()

    @allure.story("支付失败成功")
    @allure.description("支付密码错误")
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.link(url="http://test", name="链接")
    @allure.testcase(url="http://test", name="测试用例")
    @allure.issue(url="http://test", name="缺陷")
    def test_pay_error2(self):
        assert not self.pay_password_error() and self.sufficient_funds()

    @allure.story("支付失败成功")
    @allure.description("功能暂未实现")
    @allure.severity(allure.severity_level.TRIVIAL)
    @allure.link(url="http://test", name="链接")
    @allure.testcase(url="http://test", name="测试用例")
    @allure.issue(url="http://test", name="缺陷")
    @pytest.mark.xfail
    def test_pay_xfail(self):
        print("功能暂未实现")
        assert False


@allure.epic("附件")
@allure.feature("附件")
class TestAttach(object):

    def test_attach_picture(self):
        allure.attach.file(source=r"D:\pythonworkspace\TEST\pytest_test\image.PNG", name="附件名",
                           attachment_type=allure.attachment_type.PNG, extension="扩展名")

    def test_attach_html(self):
        allure.attach(body="""
        <html>
        <h1>hello world</h1>
        </html>
        """,
                      name="附件名",
                      attachment_type=allure.attachment_type.HTML,
                      extension="扩展名")


if __name__ == '__main__':
    pytest.main(["-s", "-v",
                 os.path.abspath("test_allure.py"),
                 "--clean-alluredir",
                 "--alluredir=allure-result"])
    os.system("copy environment.properties allure-result")
    os.system("allure generate allure-result -o allure-report --clean")
