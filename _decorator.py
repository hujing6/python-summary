# -*- coding: utf-8 -*-
from functools import wraps
import time


def print_time_consume(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        res = func(*args, **kwargs)
        end = time.time()
        print(f"{func.__name__} time consume: {end - start}")
        return res

    return wrapper


def print_title(**kwargs):
    """
    带参数装饰器

    :param kwargs:
    :return:
    """
    title = kwargs["title"]

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            print(f"title: {title}")
            return func(*args, **kwargs)

        return wrapper

    return decorator


class A():
    def __init__(self, name):
        self.name = name

    def instance_decorator(func):
        def wrapper(self, *args, **kwargs):
            print(f"{func.__name__}")
            return func(self, *args, **kwargs)

        return wrapper

    @instance_decorator
    def print_name(self):
        print(self.name)


@print_time_consume
def wait_time():
    time.sleep(2)


@print_title(title="两数相加")
def sum(a, b):
    return a + b


if __name__ == '__main__':
    # wait_time()
    # print(sum(a=1, b=2))
    A(name="lisa").print_name()
