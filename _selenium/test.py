# -*- coding: utf-8 -*-
"""
@time: 2023/4/10 18:01
"""

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
import time
from selenium.webdriver.support.select import Select


class element():
    driver = webdriver.Chrome("chromedriver.exe")
    # 隐式等待
    driver.implicitly_wait(10)  # seconds
    # 全屏
    driver.maximize_window()

    def _print_current(self):
        # 打印当前页标题
        print(self.driver.title)
        # 打印当前地址
        print(self.driver.current_url)

    def find_element(self):
        """
        find_element_by_id()
        find_element_by_name()
        find_element_by_class_name()
        find_element_by_tag_name()
        find_element_by_link_text()
        find_element_by_partial_link_text()
        find_element_by_xpath()
        find_element_by_css_selector()

        :return:
        """
        url = "https://www.baidu.com/"
        # 打开百度
        self.driver.get(url)
        # 通过id 找到输入框
        # element = self.driver.find_element_by_id("kw")
        element = self.driver.find_element_by_xpath('//*[@id="kw"]')
        # 输入搜索内容
        element.send_keys("selenium")
        # 回车
        element.send_keys(Keys.ENTER)

        # # 点击
        # element.find_element_by_xpath('//*[@id="2"]/div/div/h3/a').click()

        # 等待元素出现再点击
        element_click = WebDriverWait(self.driver, 10, 0.5).until(
            expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="2"]/div/div/h3/a'))
        )
        element_click.click()

        self._print_current()
        # 网页后退
        self.driver.back()
        time.sleep(2)
        self._print_current()
        # 网页前进
        self.driver.forward()
        time.sleep(2)
        self._print_current()

        # close方法是关闭当前窗口。（当前窗口的意思就是表示driver现在正在操作的窗口）如果当前窗口只有一个tab，那么这个close方法就相当于关闭了浏览器
        # self.driver.close()
        # quit方法就直接关闭是直接退出并关闭浏览器所有打开的tab窗口。所以，close方法一般关闭一个tab，quit方法才是我们认为的完全关闭浏览器方法。
        self.driver.quit()

    def mouse(self):
        """
        perform()： 执行所有 ActionChains 中存储的行为；
        context_click()： 右击；
        double_click()： 双击；
        drag_and_drop()： 拖动；
        move_to_element()： 鼠标悬停。

        :return:
        """
        url = "https://www.baidu.com/"
        # 打开百度
        self.driver.get(url)

        # 定位到要悬停的元素
        element_set = WebDriverWait(self.driver, 10, 0.5).until(
            expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="s-usersetting-top"]'))
        )
        # 对定位到的元素执行鼠标悬停操作
        ActionChains(self.driver).move_to_element(element_set).perform()
        element_click = WebDriverWait(self.driver, 10, 0.5).until(
            expected_conditions.presence_of_element_located((By.XPATH, '//*[@id="s-user-setting-menu"]/div/a[2]'))
        )
        element_click.click()
        ActionChains(self.driver).click()
        self.driver.quit()


if __name__ == '__main__':
    pass
    # element().find_element()
    # element().mouse()
