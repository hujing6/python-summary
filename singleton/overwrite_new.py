# -*- coding: utf-8 -*-
from threading import Lock


class Singleton(object):
    lock = Lock()
    init_tag = False

    def __new__(cls, *args, **kwargs):
        with cls.lock:
            if not hasattr(cls, "_instance"):
                cls._instance = super().__new__(cls)
                cls.init_tag = True
        return cls._instance

    def __init__(self, *args, **kwargs):
        pass


class A(Singleton):
    def __init__(self, *args, **kwargs):
        super(A, self).__init__(*args, **kwargs)
        self.name = kwargs["name"]
        print(self.name)


if __name__ == '__main__':
    print(id(A(name="apple")))
    print(id(A(name="pear")))
