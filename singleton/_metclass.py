# -*- coding: utf-8 -*-
from threading import Lock


class SingletonMetClass(type):
    lock = Lock()

    def __call__(cls, *args, **kwargs):
        with SingletonMetClass.lock:
            if not hasattr(cls, "_instance"):
                print("__call__")
                cls._instance = super().__call__(*args, **kwargs)
        return cls._instance

    def __new__(cls, *args, **kwargs):
        print("__new__")
        return super().__new__(cls, *args, **kwargs)


class Singleton(metaclass=SingletonMetClass):
    def __init__(self, name):
        print("__init__")
        self.name = name
        print(self.name)

if __name__ == '__main__':
    print(id(Singleton(name="apple")))
    print(id(Singleton(name="pear")))
