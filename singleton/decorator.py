# -*- coding: utf-8 -*-
from threading import Lock

"""
使用装饰器实现单例模式
"""


def singleton_decorator(cls):
    _instance_dict = {}
    lock = Lock()

    def wrapper(*args, **kwargs):
        with lock:
            if cls not in _instance_dict:
                _instance_dict[cls] = cls(*args, **kwargs)
        return _instance_dict[cls]

    return wrapper


@singleton_decorator
class Singleton():
    def __init__(self, name=""):
        self.name = name
        print(self.name)


if __name__ == '__main__':
    Singleton(name="test")
    Singleton(name="test1")
