pip install wheel
如代码所示， 指定包名， 版本号， 依赖关系，以及需要打包进去的文件。

  需要注意find_packages()方法对部分关键字取名的包如 lib ,commons 会找不到，需要手动添加；另外需要打包指定文件，需要在data_files中进行配置

+ 打包命令

  python setup.py sdist   会在当前目录dist文件文件夹下生成一个tar包

  python setup.py bdist_wheel   会在当前目录dist文件文件夹下生成一个whl包
  
+ 后续程序更新时只需要修改根目录下面的version.txt(版本号) 和requirements.txt（依赖库）