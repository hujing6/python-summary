# -*- coding: utf-8 -*-
__all__ = ["Analyse",
           "test"]

import pandas as pd
import os

class Analyse(object):
    def __init__(self, **kwargs):
        self.file = kwargs["file"]
        self.excel_data = pd.read_excel(io=self.file, sheet_name=None, header=None)

    def _get_sheet(self, **kwargs):
        sheet_name = kwargs["sheet_name"]
        return self.excel_data[sheet_name]

    def _get_table_data(self, **kwargs):
        """
        通过 表格名字 获取表格中的数据

        :param kwargs:
        :return:
        """
        table = kwargs["table"]
        sheet_name = kwargs["sheet_name"]
        sheet_data = self._get_sheet(sheet_name=sheet_name)
        sheet_data.dropna(inplace=True, how="all")
        table_name_list = list(sheet_data[0])
        if table not in table_name_list:
            raise Exception(f"not found {table}")
        table_data_start_index = table_name_list.index(table)
        table_data_start_end = len(sheet_data)
        for index, value in enumerate(table_name_list[table_data_start_index + 1:]):
            if str(value).lower().startswith("table"):
                table_data_start_end = table_data_start_index + index + 1
                break
        table_data = sheet_data.iloc[table_data_start_index + 1:table_data_start_end, :]
        return table_data

    def get_data_key_use_first_row(self, **kwargs):
        """
        获取数据，以第一行的值为key

        :param kwargs:
        :return:
        """
        table = kwargs["table"]
        sheet_name = kwargs["sheet_name"]
        table_data = self._get_table_data(table=table, sheet_name=sheet_name)
        table_data.columns = table_data.iloc[0]
        return list(table_data.iloc[1:].T.to_dict().values())

    def get_data_key_use_first_col(self, **kwargs):
        """
        获取数据，以第一行的值为key

        :param kwargs:
        :return:
        """
        table = kwargs["table"]
        sheet_name = kwargs["sheet_name"]
        table_data = self._get_table_data(table=table, sheet_name=sheet_name)
        return dict(zip(list(table_data[0]), list(table_data[1])))


def test():
    analyse_data = Analyse(file=os.path.join(os.path.dirname(__file__), "data.xlsx"))
    res = analyse_data.get_data_key_use_first_col(table="Table:first_col", sheet_name="data")
    print(res)


if __name__ == '__main__':
    test()
    # analyse_data = Analyse(file=r"../data.xlsx")
    # res = analyse_data.get_data_key_use_first_col(table="Table:first_col", sheet_name="data")
    # print(res)
    # res = analyse_data.get_data_key_use_first_row(table="Table:first_row", sheet_name="data")
    # print(res)
