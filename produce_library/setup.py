# -*- coding: utf-8 -*-

from pathlib2 import Path
from setuptools import setup, find_packages

# 读取版本号
version_path = Path(".", "version.txt")
current_version = version_path.read_text()

# 读取依赖
require_list = list()
require_path = Path(".", "requirements.txt")
with require_path.open() as f:
    for line in f.readlines():
        require_list.append(line.strip())

all_pkg = find_packages()
# find_packages 方法找不到包，手动添加
# all_pkg.append("")
setup(
    name='analyse_excel',  # 应用名
    version=current_version,  # 版本号
    packages=all_pkg,  # 包括在安装包内的 Python 包
    data_files=[("analyse_excel", ["version.txt", "requirements.txt"])],
    package_data={'analyse_excel': ['data.xlsx']},
    install_requires=require_list,
)
