# -*- coding: utf-8 -*-
"""
@time: 2023/4/18 21:03
"""
"""
?:
?=
?!
?<=
?<!
使用
"""

import re

# ?:, 匹配xx前面的,包含xx
re.search(r"123(?:456)", "123456")
# <_sre.SRE_Match object; span=(0, 6), match='123456'>
re.search(r"123(?:456)", "124456")
# None

# ?=, 匹配xx前面的,不包含xx
re.search(r"123(?=456)", "123456")
# <_sre.SRE_Match object; span=(0, 6), match='123'>
re.search(r"123(?=456)", "124456")
# None

# ?!, 匹配非xx前面的
re.search(r"123(?!456)", "123456")
# None
re.search(r"123(?!456)", "123455")
# <_sre.SRE_Match object; span=(0, 3), match='123'>

# ?<:, 匹配xx后面的,包含xx
re.search(r"(?<=123)456", "123456")
# <_sre.SRE_Match object; span=(3, 6), match='456'>
re.search(r"(?<=123)456", "123457")
# None
re.search(r"(?<=123).*", "123457")
# <_sre.SRE_Match object; span=(3, 6), match='457'>

# ?!, 匹配非xx后面的
re.search(r"(?<!123)456", "123456")
# None
re.search(r"(?<!123)456", "12456")
# <_sre.SRE_Match object; span=(2, 5), match='456'>