# -*- coding: utf-8 -*-
"""
super(type[,object-or-type])

type: 类
object-or-type: 类，一般为self

1. MRO method resolution order 方法解析顺序，解决多继承问题
2. 调用父类的方法
"""
#
#
# class A():
#     def test(self):
#         print('A')
#
#
# class B():
#     def test(self):
#         print('B')
#
#
# class D():
#     def test(self):
#         print('D')
#
#
# class C(A, D, B):
#     def __init__(self):
#         super().test()
#         super(C, self).test()
#         super(A, self).test()
#         super(D, self).test()
#

# C()
"""
A
A
D
B
"""


class A():
    def __init__(self):
        print("init A")

    def who_am_i(self):
        print(f"I am A")


class B(A):
    def __init__(self):
        super().__init__()
        print("init B")

    def who_am_i(self):
        print(f"I am B")


class C(A):
    def __init__(self):
        super().__init__()
        print("init C")

    def who_am_i(self):
        print(f"I am C")


# 先初始化父类再初始化字类，顺序从右往左
class D(B, C):
    def __init__(self):
        super().__init__()
        print("init D")


D().who_am_i()
