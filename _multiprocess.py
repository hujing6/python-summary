# -*- coding: utf-8 -*-
"""
@time: 2023/3/29 1:27
"""
from multiprocessing import Process, Pool, Queue, Manager
import os
import time
# torch.multiprocessing.Queue 多进程中使用
# queue.Queue 多线程中使用， 使用到了线程锁
# multiprocessing.Manager()中的Queue() 进程池使用


def test(name, q):
    number = q.get()
    print(f"message: i am {number}")
    q.put(number + 1)
    print(f"{name}: pid: {os.getpid()}--{time.time()}")
    time.sleep(2)
    return number


def call_back(*args, **kwargs):
    print("call back", *args, **kwargs)


if __name__ == '__main__':
    # 1
    # 进程通信
    q = Queue()
    q.put(1)
    pList = []
    for i in range(20):
        p = Process(target=test, kwargs={"name": f"task {i}", "q": q})
        # pList.append(p)
        p.start()
        # p.join() # 是否等待进程实例执行结束，或等待多少秒。
        # p.terminate() # 不管任务是否完成，立即终止。
        # p.kill()
    for p in pList:
        p.join()
    # # 2 ， 进程池
    poll = Pool(4)
    # 获取进程返回值
    result_list = []
    q = Manager().Queue()
    q.put(1)
    for i in range(5):
        # 非阻塞
        result_list.append(poll.apply_async(func=test, kwds={"name": f"task {i}", "q": q}, callback=call_back))
        # # 阻塞
        # poll.apply(func=test, kwds={"name": f"task {i}", "q": q})
    poll.close()  # close 不能再apply
    poll.join()  # #等待进程池里面的进程运行完
    for res in result_list:
        print(f"res: {res.get()}")

