# -*- coding: utf-8 -*-
"""
@time: 2023/4/11 17:55
"""

import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", required=True)

    kwargs = parser.parse_args()
    print(f"name {kwargs.name}")
