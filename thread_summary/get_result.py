# -*- coding: utf-8 -*-
from threading import Thread
import time

"""
线程-获取方法结果
"""


class MyThread(Thread):
    result = None

    def __init__(self, *args, **kwargs):
        super(MyThread, self).__init__(*args, **kwargs)

    def run(self) -> None:
        self.result = self._target(**self._kwargs)

    def get_result(self):
        return self.result


def sleep(**kwargs):
    wait = kwargs["wait"]
    time.sleep(int(wait))
    return wait


if __name__ == '__main__':
    th = MyThread(target=sleep, kwargs={"wait": 1})
    th.start()
    th.join()
    result = th.get_result()
    print(f"result: {result}")
