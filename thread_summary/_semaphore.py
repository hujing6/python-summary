# -*- coding: utf-8 -*-

from threading import Thread, Semaphore
import time

"""
信号
"""


class MyThread(Thread):
    semaphore = Semaphore(3)
    result = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def run(self):
        self.semaphore.acquire()
        print(f"{self.ident} running")
        time.sleep(2)
        self.result = test(self.ident)
        print(f"{self.ident} finish")
        self.semaphore.release()

    def get_result(self):
        return self.result


def test(pid):
    print("execute func test")
    return pid


if __name__ == '__main__':
    th_list = []
    for i in range(10):
        th = MyThread()
        th.start()
        th_list.append(th)
    for th in th_list:
        th.join()
        pid = th.get_result()
        print(f"end {pid}")
