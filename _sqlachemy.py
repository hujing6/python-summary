# -*- coding: utf-8 -*-

import logging
import threading
from threading import Lock

from sqlalchemy import Column, Integer, String, Enum
from sqlalchemy import create_engine
from sqlalchemy.engine.row import Row
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
import math


class SingletonMetaClass(type):
    _instance_lock = threading.Lock()

    def __init__(self, *args, **kwargs):
        super(SingletonMetaClass, self).__init__(*args, **kwargs)
        self.__instance = None

    def __call__(self, *args, **kwargs):
        with self._instance_lock:
            if self.__instance is None:
                self.__instance = super(SingletonMetaClass, self).__call__(*args, **kwargs)
        return self.__instance


class ConnectEngine(metaclass=SingletonMetaClass):
    def __init__(self):
        engine_url = "mysql+pymysql://root:123.com@192.168.234.241:3306"
        self.engine = create_engine(url=f'{engine_url}/device', isolation_level='AUTOCOMMIT', encoding='utf-8',
                                    pool_size=5
                                    )
        logging.debug(f"create engine")


class Session():
    session = None

    def __enter__(self):
        # self.session = sessionmaker(bind=ConnectEngine().engine)()
        self.session = scoped_session(sessionmaker(bind=ConnectEngine().engine))()
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_val:
            self.session.rollback()
            logging.error(f"exec error {exc_val}")
        self.session.commit()
        self.session.close()
        engine = ConnectEngine().engine
        logging.debug(f"空闲：{engine.pool.checkedin()}")
        logging.debug(f"占用：{engine.pool.checkedout()}")


class KFResult():
    def __init__(self, **kwargs):
        self.status = kwargs["status"]
        self.result = kwargs["result"]

    def __str__(self):
        return f"status: {self.status}, result: {self.result}"

    def __repr__(self):
        return self.__str__()


class Device(declarative_base()):
    # 指定表明
    __tablename__ = "device"  #

    deviceNumber = Column(Integer, autoincrement=True, primary_key=True)
    project = Column(String(100), nullable=False)
    userName = Column(String(20), nullable=True, default="")
    comPort = Column(String(3), nullable=True, default='')
    p1Port = Column(String(3), nullable=True, default="")
    rs485Port = Column(String(20), nullable=True, default='')
    serverIpAddress = Column(String(20), nullable=True, default="")
    serverIpPort = Column(String(5), nullable=True, default="")
    sapId = Column(String(5), nullable=True, default="")
    meterNo = Column(String(50), nullable=True, default="")
    # ctrlHost = Column(String(20), nullable=True, default="")
    # ctrlIndex = Column(String(10), nullable=True, default="")
    powerOnEnv = Column(String(50), nullable=True, default="")
    benchType = Column(String(10), nullable=True, default="")
    benchAddr = Column(String(20), nullable=True, default="")
    status = Column(String(10), nullable=False, default="idle")
    communicationEnv = Column(String(100), nullable=False, default="")
    updateTime = Column(String(10), nullable=True, default="")
    hasRobot = Column(Enum('Y', 'N'), nullable=False, default='N')
    meterType = Column(String(20), nullable=False, default="")
    remark = Column(String(255), nullable=True, default="")

    def __repr__(self):
        return f"<projects(project={self.project}, deviceNumber={self.deviceNumber}>"


class DBOption(object):
    def __init__(self, **kwargs):
        self.engine = kwargs["engine"]

    def __enter__(self):
        self.session_maker = scoped_session(sessionmaker(bind=self.engine))
        self.session = self.session_maker()
        return self.session

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_val:
            self.session.rollback()
            logging.error(f"exec error {exc_val}")
        # 使用 scoped_session, 会话对象先取消注册，再退出
        self.session.commit()
        self.session.close()
        self.session_maker.remove()
        engine = ConnectEngine().engine
        logging.debug(f"空闲：{engine.pool.checkedin()}")
        logging.debug(f"占用：{engine.pool.checkedout()}")


class BaseDB(object):
    table = Device
    engine = ConnectEngine().engine
    queryResultItemDescr = ["object"]

    def queryFilterNone(func):
        """
        更新查询条件中的None

        :return:
        """

        def wrapper(cls, *args, **kwargs):
            if "query" in kwargs:
                query = kwargs["query"]
                query = {key: value for key, value in query.items() if value is not None}
                kwargs["query"] = query
            return func(cls, *args, **kwargs)

        return wrapper

    @classmethod
    def _queryObjectListToDictList(cls, data, itemDescr):
        """
        将查询结果集合转为字典列表

        :param data:    转换源数据
        :param itemDesc:    子对象描述
        :return:
        """

        def obejectToDict(object):
            value = {}
            for attr, va in object.__dict__.items():
                if not str(attr).startswith("_"):
                    value[attr] = va
            return value

        response = []
        for itemValue in data:
            value = {}
            if not isinstance(itemValue, Row):
                value = obejectToDict(itemValue)
            else:
                for index, item in enumerate(itemValue):
                    descr = itemDescr[index]
                    if descr == "object":
                        value.update(obejectToDict(item))
                    else:
                        value[descr] = item
            response.append(value)
        data.session.close()
        return response

    @classmethod
    def insert(cls, **kwargs):
        """
        插入一条数据

        :param kwargs: 数据
        :return:
        """
        with DBOption(engine=cls.engine) as session:
            data = cls.table(**kwargs)
            session.add(data)
        return KFResult(status=True, result=kwargs)

    @classmethod
    @queryFilterNone
    def delete(cls, **kwargs):
        """
        删除查询的数据

        :param kwargs:
        :param query:   查询条件
        :return:
        """
        query = kwargs["query"]
        with DBOption(engine=cls.engine) as session:
            currentValue = session.query(cls.table).filter_by(**query)
            currentValue.delete()
        return KFResult(status=True, result=f"delete ok")

    @classmethod
    @queryFilterNone
    def update(cls, **kwargs):
        """
        更新满足查询条件的数据

        :param kwargs:
        :param query:   查询条件
        :param data:    更新数据
        :return:
        """
        query = kwargs["query"]
        data = kwargs["data"]
        data = {key: value for key, value in data.items() if value is not None}
        if len(data) == 0:
            return KFResult(status=True, result=data)
        with DBOption(engine=cls.engine) as session:
            currentValue = session.query(cls.table).filter_by(**query)
            if currentValue.count() == 0:
                return KFResult(status=False, result=f"not exists")
            currentValue.update(data)
        return KFResult(status=True, result=data)

    @classmethod
    @queryFilterNone
    def isExist(cls, **kwargs):
        """
        判断是否有满足查询条件的记录

        :param kwargs:
        :param query:   查询条件
        :return:
        """
        query = kwargs["query"]
        with DBOption(engine=cls.engine) as session:
            value = session.query(cls.table).filter_by(**query)
            if value.count() == 0:
                return False
        return True

    def __pageQuery(func):
        def wrapper(cls, *args, **kwargs):
            pageSize = None
            pageNo = None
            if "pageSize" in kwargs:
                pageSize = kwargs.get("pageSize")
            if "pageNo" in kwargs:
                pageNo = kwargs.get("pageNo")
            if None in [pageSize, pageNo]:
                return func(cls, *args, **kwargs)
            else:
                return cls._queryWithPage(*args, **kwargs)

        return wrapper

    @classmethod
    @queryFilterNone
    @__pageQuery
    def query(cls, **kwargs):
        """
        查询表

        :param kwargs:
        :return:
        """
        queryObject = cls._queryObject(**kwargs)
        response = cls._queryObjectListToDictList(data=queryObject, itemDescr=cls.queryResultItemDescr)
        return KFResult(status=True, result=response)

    @classmethod
    def _queryWithPage(cls, **kwargs):
        """
        分页查询

        :param kwargs:
        :param query:   查询条件
        :param pageSize:    页大小
        :param pageNo:  页码
        :param attribute:   查询条件后的条件过滤
        :return:
        """
        pageSize = int(kwargs["pageSize"])
        pageNo = int(kwargs["pageNo"])
        queryObject = cls._queryObject(**kwargs)
        totalRow = cls._pageTotalRow(query=queryObject)
        pageQuery = queryObject.offset((pageNo - 1) * pageSize).limit(pageSize)
        pageData = cls._queryObjectListToDictList(data=pageQuery, itemDescr=cls.queryResultItemDescr)
        totalPage = math.ceil(totalRow / pageSize)
        response = {
            "pageNo": pageNo,
            "pageSize": pageSize,
            "totalRow": totalRow,
            "totalPage": totalPage,
            "pageData": pageData
        }
        return KFResult(status=True, result=response)

    @classmethod
    def _queryObject(cls, **kwargs):
        """
        返回查询对象

        :param kwargs:
        :param query:   查询条件
        :return:
        """
        query = kwargs["query"]
        with DBOption(engine=cls.engine) as session:
            return session.query(cls.table).filter_by(**query)

    @classmethod
    def _pageTotalRow(cls, query):
        """
        统计查询对象结果总体条数

        :param query:   查询对象
        :return:
        """
        return query.count()

    @classmethod
    def insertInexistentData(cls, **kwargs):
        """
        插入不存在的多条数据

        :param kwargs:
        :param data:    插入的数据，数据类型是字典列表
        :return:
        """
        data = kwargs["data"]
        with DBOption(engine=cls.engine) as session:
            session.execute(
                cls.table.__table__.insert(),
                data
            )

    @classmethod
    def insertOrUpdateRow(cls, **kwargs):
        """
        新增或者更新一条数据

        :param kwargs:
        :param data:    数据，数据类型：字典
        :return:
        """
        data = kwargs["data"]
        tableClass = cls.table
        with DBOption(engine=cls.engine) as session:
            row = tableClass(**data)
            session.merge(row)


def query():
    with Session() as session:
        result = session.execute("select * from device")
    # time.sleep(1)
    # result.all()
    logging.info(f"######### execute sql ########")


def execute_query():
    try:
        query()
    except Exception as er:
        logging.error(f"{er}")

# if __name__ == '__main__':
#     for i in range(1000000):
#         tr = threading.Thread(target=BaseDB.query, kwargs={"query": {}})
#         tr.start()
