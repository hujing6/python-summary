# -*- coding: utf-8 -*-
import json
from docx import Document
from docx.styles import style, styles
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.shared import Pt, Inches


def getData():
    with open("report.json", mode="r", encoding="utf-8") as fp:
        data = json.load(fp)
    return data["data"]


def writeDoc(data):
    doc = Document()
    for key, value in data.items():
        keyType = key.split("#")[0]
        if keyType == "head":
            subValue = value["value"]
            level = value.get("level", 1)
            head = doc.add_heading(subValue, level=level)
            head.style.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            head.style.font.size = Inches(0.5)
            # head.style
        elif keyType == "paragraph":
            doc.add_paragraph(value["value"])
        elif keyType == "table":
            #       "title": "成功率",
            #       "rowName": ["项目名", "成功率", "备注"],
            #       "data": [
            #         ["tulip", "100%", "抄读结果良好"],
            #         ["camel", "50%", "通讯一般"]
            #       ]
            tableTitle = value["title"]
            tableData = value["data"]
            # 添加表格名
            paragraph = doc.add_paragraph(tableTitle, style='List Number')
            paragraph.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
            paragraph.style.font.size = Pt(14)
            #
            rowLen = len(tableData[0])
            colLen = len(tableData)
            table = doc.add_table(rowLen, colLen)
            # table.style = "Medium Shading 2 Accent 3"
            # table.style = "Table Grid"
            # table.style = "Light Grid Accent 3"
            table.style = "Light List Accent 3"
            for index, colValueList in enumerate(tableData):
                hdr_cells = table.columns[index].cells
                for subindex, colVa in enumerate(colValueList):
                    hdr_cells[subindex].text = colVa
    doc.save("report.docx")


if __name__ == '__main__':
    data = getData()
    writeDoc(data)
