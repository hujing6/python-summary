# -*- coding: utf-8 -*-
"""
@time: 2023/4/11 18:00
"""

import logging
from logging.handlers import RotatingFileHandler

logger = logging.getLogger()
# DEBUG<INFO<WARNING<ERROR
# 打印>=指定等级的
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter("%(asctime)s: %(message)s")

# 控制台输出
streamHandler = logging.StreamHandler()
streamHandler.setFormatter(formatter)

# 文件输出，文件最大为10B，否则创建新的文件log.log.1、log.log.2、log.log.3
fileHandler = RotatingFileHandler('log.log', maxBytes=10, backupCount=3)
fileHandler.setFormatter(formatter)

logger.addHandler(streamHandler)
logger.addHandler(fileHandler)

for i in range(100):
    logging.debug("debug")
    logging.info("info")
    logging.error("error")
    logging.warning("warning")

# level DEBUG
# 2023-04-11 18:14:55,451: debug
# 2023-04-11 18:14:55,451: info
# 2023-04-11 18:14:55,451: error
# 2023-04-11 18:14:55,452: warning

# level INFO
# 2023-04-11 18:14:20,397: info
# 2023-04-11 18:14:20,397: error
# 2023-04-11 18:14:20,397: warning

# level WARNING
# 2023-04-11 18:15:28,641: error
# 2023-04-11 18:15:28,642: warning

# level ERROR
# 2023-04-11 18:15:28,641: error
