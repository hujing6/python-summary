# -*- coding: utf-8 -*-
import os
import yaml
import argparse

if __name__ == '__main__':
    argument = argparse.ArgumentParser()
    argument.add_argument("--name")
    item = argument.parse_args()
    name = item.name
    f = open(os.path.join(os.path.dirname(__file__), "static/user.yaml"), mode="r")
    item = yaml.load(f, yaml.FullLoader)
    print(item.get(name))
