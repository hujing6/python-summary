pip install pyinstaller

1. pyinstaller XXX.py
    在 dst 目录下生成.exe 文件

2. pyinstaller XXX.spec

    生成spec文件

    * pyi-makespec main.py

    * pyi-makespec -F main.py

* 单文件模式：加上-F参数，全部的依赖文件都会被打包到exe文件中，在dist文件夹中只有一个可执行文件，把这个可执行文件发给别人就可以直接运行了。

  pyinstaller XXX.py -F

* 文件夹模式：dist文件夹储存可执行文件即相关依赖，这种模式下，需要把整个dist文件夹发给别人才能运行。
	* build文件夹用于存储日志文件。
	* dist文件夹储存可执行文件即相关依赖。
	* \__pycache\__文件夹里是Python版本信息。
	* fileren.spec打包的配置文件，可以配置依赖资源。



spec 文件
# datas=[] 一个空的list，把要添加的资源文件以tuple的形式传入。
# tuple的第一个元素是资源文件的路径，第二个元素是打包后存放资源的文件夹

#### 文件夹模式
```
# -*- mode: python ; coding: utf-8 -*-


block_cipher = None

a = Analysis(['main.py'],
             pathex=[],
             binaries=[],
             datas=[("XMLFunctionLists.xml", "."), ("readme.txt", ".")],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='main',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='main')

```

#### 文件模式
```
# -*- mode: python ; coding: utf-8 -*-


block_cipher = None


a = Analysis(['main.py'],
             pathex=[],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='main',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None )
```