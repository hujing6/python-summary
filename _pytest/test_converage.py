# -*- coding: utf-8 -*-
import pytest

# 安装pytest-cov
pytest.main(["-s", "-v", "xxx",
             '--html=reports/report.html',
             "--self-contained-html",
             f"--cov=xxx",
             "--cov-report=html",
             f"--cov-config=.coveragerc"
             ])
# --cov 代码路径
# --cov-config 跳过某些脚本的覆盖率测试
