# -*- coding: utf-8 -*-
import os

import pytest

"""
筛选 类名和 方法名: 不区分大小写, 模糊匹配
"""


class TestApple():
    def test_apple(self):
        print("class Apple: apple")

    def test_orange(self):
        print("class Apple: orange")


class TestOrange():
    def test_orange(self):
        print("class Orange: orange")

    def test_apple(self):
        print("class Orange: apple")


if __name__ == '__main__':
    # 类或者方法任意匹配到ple执行
    pytest.main(["-s", "-v", "-k", "ple", os.path.abspath("test_filter.py")])

    # 类或者方法匹配到ple不包含test_a
    # pytest.main(["-s", "-v", "-k", "ple and not test_a", os.path.abspath("test_filter.py")])
    # 类或者方法匹配到ple 或者 apple
    # pytest.main(["-s", "-v", "-k", "ple or apple", os.path.abspath("test_filter.py")])
