# -*- coding: utf-8 -*-
import os
import pytest

skip = False


@pytest.mark.skip
def test_skip():
    print("跳过此用例")


@pytest.mark.skipif(skip, reason="")
def test_skip():
    print("如果 skip 为 True 跳过此用例")


def test_function():
    print("function")


if __name__ == '__main__':
    pytest.main(["-s", "-v", os.path.abspath("test_skip.py")])
