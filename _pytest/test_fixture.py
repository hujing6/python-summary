# -*- coding: utf-8 -*-
import pytest
import os

# autouse 自动调用fixture功能
@pytest.fixture(scope='function', autouse=True)  # 声明为fixture(测试准备/清理)方法
def setup_func():
    print("function start")
    yield
    print("function end")


@pytest.fixture(scope='module', autouse=True)  # 声明为fixture(测试准备/清理)方法
def setup_module():
    print("module start")
    yield
    print("module end")


@pytest.fixture(scope='class', autouse=True)  # 声明为fixture(测试准备/清理)方法
def setup_class():
    print("class start")
    yield
    print("class end")


@pytest.fixture(scope='session', autouse=True)  # 声明为fixture(测试准备/清理)方法
def setup_session():
    print("session start")
    yield 'session start'
    print("session end")

def test_func():
    print("execute func")


if __name__ == '__main__':
    pytest.main(["-s", "-v", os.path.abspath("test_fixture")])
