# -*- coding: utf-8 -*-
import os

import pytest


def setup_module():
    print("模块开始执行")


def teardown_module():
    print("模块结束执行")


def setup_function():
    print("每个模块级方法（不在类内）开始执行")


def teardown_function():
    print("每个模块级方法（不在类内）结束执行")


def test_function():
    print("模块级方法（不在类内）")


def test_function1():
    print("模块级方法（不在类内）1")


class TestClassName(object):
    def setup_class(self):
        print("类开始执行")

    def teardown_class(self):
        print("类结束执行")

    def setup_method(self):
        print("每个类方法开始执行")

    def teardown_method(self):
        print("每个类方法结束执行")

    def test_functionName(self):
        print(f"类方法")


if __name__ == '__main__':
    pytest.main(["-s", "-v", os.path.abspath("test_set_up.py")])
    # -m=xxx: 运行打标签的用例
    # -reruns=xxx，失败重新运行
    # -q: 安静模式, 不输出环境信息
    # -v: 丰富信息模式, 输出更详细的用例执行信息
    # -s: 显示程序中的print/logging输出
    # --resultlog=./log.txt 生成log
    # --junitxml=./log.xml 生成xml报告
    # --html=./clockOperation.html 生成html报告，需要插件pytest-html
    # -x: 遇到错误停止执行
    # --maxfil: 遇到几个错结束 --maxfil=3
    # --self-contained-html html 样式包含在html中
