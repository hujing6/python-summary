# -*- coding: utf-8 -*-
import os

import pytest

"""
参数化

"""


class TestParametrize1():
    sumTestData = [(3, eval("1+2")),
                   (4, eval("1+3"))]

    @pytest.mark.parametrize("input,expect", sumTestData)
    def test_sum(self, input, expect):
        assert input == expect


class TestParametrize2():
    test_user_Data = [{'name': "lili", "age": 18}, {'name': "jack", "age": 20}]

    @pytest.fixture(scope="function")
    def login(self, request):
        name = request.param['name']
        age = request.param['age']
        print(f"login: {name}")
        return age

    # indirect = True ,login 作为方法调用
    @pytest.mark.parametrize("login", test_user_Data, indirect=True)
    def test_user(self, login):
        print(f"age : {login}")


class TestParametrize3():
    test_user_Data = [{'name': "lili", "age": 18}, {'name': "jack", "age": 20}]
    test_score_Data = [{'score': 89}, {'score': 90}]

    @pytest.fixture(scope="function")
    def login(self, request):
        name = request.param['name']
        age = request.param['age']
        print(f"login: {name}")
        return age

    @pytest.fixture(scope="function")
    def score(self, request):
        score = request.param['score']
        print(f"score: {score}")
        return score

    # indirect = True ,login 作为方法调用
    @pytest.mark.parametrize("score", test_score_Data, indirect=True)
    @pytest.mark.parametrize("login", test_user_Data, indirect=True)
    def test_user(self, login, score):
        assert score < 85


if __name__ == '__main__':
    # -x 遇到失败则停止
    # -maxfail 失败2个则停止
    pytest.main(["-s", "-v", "-x", "--maxfail", "2", os.path.abspath("test_parametrize.py")])
