# -*- coding: utf-8 -*-
import os

import pytest

"""
期望测试结果是的失败，但是不影响测试执行

"""


@pytest.mark.xfail
def test_xfail():
    print(f"功能尚未完成")

    assert 1 == 2


def test_fail():
    assert 1 == 2


if __name__ == '__main__':
    pytest.main(["-s", "-v", os.path.abspath("test_xfail.py")])
