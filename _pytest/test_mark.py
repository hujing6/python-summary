# -*- coding: utf-8 -*-
import os

import pytest

"""
根据标签筛选脚本
"""


@pytest.mark.apple
def test_apple1():
    print("apple 1")


@pytest.mark.apple
def test_apple2():
    print("apple 2")


@pytest.mark.orange
def test_orange1():
    print("orange 1")


@pytest.mark.orange
def test_orange2():
    print("orange 2")


if __name__ == '__main__':
    # 执行标签为apple的用例
    pytest.main(["-s", "-v", os.path.abspath("test_mark.py"), "-m=apple"])
    # 执行标签不为apple的用例
    # # pytest.main(["-s", "-v", os.path.abspath("test_mark.py"), "-m=not apple"])
    # 执行标签为apple或者orange的用例
    # pytest.main(["-s", "-v", os.path.abspath("test_mark.py"), "-m=apple or orange"])
