# -*- coding: utf-8 -*-
__all__ = ["logging_formatter"]
import logging
import os
from logging.handlers import RotatingFileHandler
import shutil

logging_formatter = logging.Formatter(fmt="%(asctime)s[%(levelname)s]: %(message)s")
logger = logging.getLogger(name="")


def config_logger():
    logger.level = logging.DEBUG
    log_dir = os.path.join(os.path.dirname(__file__), "logs")
    if os.path.exists(log_dir):
        shutil.rmtree(log_dir)
    os.mkdir(log_dir)
    filename = os.path.join(log_dir, "log.log")
    streamHandler = logging.StreamHandler()
    streamHandler.formatter = logging_formatter
    logger.addHandler(streamHandler)
    # 日志大于10M 则进行分割
    fileHandler = RotatingFileHandler(filename=filename, encoding="utf-8", maxBytes=10 * 1024 * 1024)
    # fileHandler = logging.FileHandler(filename=filename, encoding="utf-8")
    fileHandler.formatter = logging_formatter
    logger.addHandler(fileHandler)