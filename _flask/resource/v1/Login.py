# -*- coding: utf-8 -*-
from flask_restful import Resource
from comm import response_format
from jwt import encode
import time
from config.jwt import JWT_PREFIX, JWT_SECRET, JWT_EXPIRATION_DELTA, JWT_ALGORITHM


def create_token(identify):
    identify.update({
        "exp": time.time() + JWT_EXPIRATION_DELTA,
    })
    token = encode(identify, key=JWT_SECRET, algorithm=JWT_ALGORITHM).decode("utf-8")
    return f"{JWT_PREFIX}{token}"


user = {
    "admin": {"password": "123", "role": "admin"},
    "guest": {"password": "1234", "role": "guest"}
}


class Login(Resource):
    @response_format
    def post(self, **kwargs):
        """
        登录
        ---
        tags:
          - loin
        parameters:
          - name: name
            in: formData
            description: 用户名
            type: string
            enum: ['admin']
            required: true
          - name: password
            in: formData
            description: 密码
            type: string
            enum: ['123']
            required: true

        responses:
          200:
            description: 登录成功


            example: {'code':200,'message':‘’}
          400:
            description: 登录失败


            example: {'code':200,'message':‘’, "data": {"token":b''}}

    """
        name = kwargs["name"]
        password = kwargs["password"]
        if name not in user:
            response = {"code": 400, "message": "用户不存在"}
        elif user[name]["password"] != password:
            response = {"code": 400, "message": "密码错误"}
        else:
            identify = {"name": name, "role": user[name]["role"]}
            response = {"code": 200, "message": "", "data": {"token": create_token(identify=identify)}}

        return response
