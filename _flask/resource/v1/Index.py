# -*- coding: utf-8 -*-
from flask_restful import Resource
from comm import response_format
from _jwt_decorator import jwt_required, current_identity


class Index(Resource):
    @response_format
    @jwt_required
    def get(self, **kwargs):
        """
        登录
        ---
        tags:
          - index
        security:
          - APIKeyHeader: []
        responses:
          200:
            description: 认证成功


            example: {'code':200,'message':‘’}
          400:
            description: 认证失败


            example: {'code':200,'message':‘’}

        """
        response = {"code": 200, "message": "",
                    "data": {"name": current_identity.get("name"), "role": current_identity.get("role")}}
        return response
