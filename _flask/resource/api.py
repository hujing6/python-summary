# -*- coding: utf-8 -*-
from .v1 import *
from flask_restful import Api


api = Api(prefix="/v1")
api.add_resource(Login, "/login")
api.add_resource(Index, "/index")