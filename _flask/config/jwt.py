# -*- coding: utf-8 -*-
JWT_SECRET = "secret"
JWT_EXPIRATION_DELTA = 24 * 3600
JWT_PREFIX = "Bearer "
JWT_ALGORITHM = 'HS256'
