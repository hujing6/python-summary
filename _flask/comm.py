# -*- coding: utf-8 -*-
from flask import request, jsonify
import logging
from functools import wraps


def get_request_para():
    para = {}
    try:
        # 带在路径中的查询参数 ？name=123
        res = request.args.to_dict()
        para.update(res)
        logging.info(f"request args: {res}")
    except Exception as er:
        pass
    try:
        # 当请求头content-type 是 application/x-www-form-urlencoded 或者是 multipart/form-data 时
        res = request.form.to_dict()
        para.update(res)
        logging.info(f"request form: {res}")
    except Exception as er:
        pass
    try:
        # form表单需要指定 enctype为 multipart/form-data
        res = request.files.to_dict()
        para.update(res)
        logging.info(f"request files: {res}")
    except Exception as er:
        pass
    try:
        # json
        res = request.json
        if res:
            para.update(res)
            logging.info(f"request json: {res}")
    except Exception as er:
        pass
    return para


def response_format(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        para = get_request_para()
        response = func(self, *args, **kwargs, **para)
        if isinstance(response, dict):
            response = jsonify(response)
        logging.info(f"response: {response.data}")
        return response

    return wrapper
