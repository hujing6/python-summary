## 安装 mod_wsgi
https://www.lfd.uci.edu/~gohlke/pythonlibs/

注意 Python 版本和系统位数
1. pip install mod_wsgi-4.7.1-cp36-cp36m-win_amd64.whl
2. 记录 "mod_wsgi-express module-config" 输出的内容
mod_wsgi-express module-config

	LoadFile "d:/softinstall/python/python36.dll"
	LoadModule wsgi_module "d:/softinstall/python/lib/site-packages/mod_wsgi/server/mod_wsgi.cp36-win_amd64.pyd"
	WSGIPythonHome "d:/softinstall/python"
## 安装 apache 及配置
注意系统位数要和Python一致

https://www.apachelounge.com/download/#google_vignette
1. 修改 httpd.conf 文件
	Define SRVROOT "\Apache24(实际Apache路径)"
	ServerName localhost
2. 启动apache进行测试
然后打开cmd，cd到“c:\Apache24\bin”，运行“httpd –k install”安装Apache服务，顺便启动它。（常用命令：运行服务httpd –k start，停止服务 httpd –k stop ，也可以直接从服务器管理器中操作）
浏览器输入 localhost ，显示 "It Works!” 说明 Apache 服务启动成功

## 在apache 中配置flask 项目
1. 修改 httpd.conf 文件
	1. 添加
	LoadFile "d:/softinstall/python/python36.dll"
	LoadModule wsgi_module "d:/softinstall/python/lib/site-packages/mod_wsgi/server/mod_wsgi.cp36-win_amd64.pyd"
	WSGIPythonHome "d:/softinstall/python"
	2. 去掉注释
	Include conf/extra/httpd-vhosts.conf
2. 修改httpd-vhosts.conf
	1. 去掉原有的VirtualHost
	2. 添加
	```
	<VirtualHost *:80>
    # 设置一个邮件地址，如果服务器有任何问题将发信到这个地址， 这个地址会在服务器产生的某些页面中出现
    ServerAdmin webmaster@dummy-host2.example.com
    #指定网站根目录
    DocumentRoot "${FLASKROOT}"
    #指定网站用localhost:81来访问
    ServerName localhost:81
    #这条命令就是指定对 localhost:81/.. 的访问都转由${FLASKROOT}/flaskApp.wsgi来处理
    WSGIScriptAlias / "${FLASKROOT}/flaskApp.wsgi"
    ErrorLog "logs/flaskApp.com-error.log"
    CustomLog "logs/flaskApp.com-access.log" common
    #指定目录下的文件都可访问
    <Directory "${FLASKROOT}">
        AllowOverride None
        Require all granted
    </Directory>
</VirtualHost>
```

flaskApp.wagi
```
# -*- coding: utf-8 -*-
import sys

sys.path.insert(0, 'xx\project')
from main import app as application  # wsgi只认这个application
```
3. 重新启动服务