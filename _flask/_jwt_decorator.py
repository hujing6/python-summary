# -*- coding: utf-8 -*-
from functools import wraps
from flask import request, _request_ctx_stack, current_app
from werkzeug.local import LocalProxy
from config.jwt import JWT_PREFIX, JWT_SECRET, JWT_ALGORITHM
from jwt import decode

current_identity = LocalProxy(lambda: getattr(_request_ctx_stack.top, 'current_identity', None))


def jwt_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        authorization = request.headers.get("Authorization")
        if authorization is None:
            return {"code": 400, "message": "Authorization is requirement"}
        token = authorization
        if not str(token).startswith(JWT_PREFIX):
            return {"code": 400, "message": "token error"}
        else:
            token = str(token)[len(JWT_PREFIX):].encode("utf-8")
            try:
                identify = decode(token, JWT_SECRET, algorithms=JWT_ALGORITHM)
                exp = identify["exp"]
            except Exception as er:
                return {"code": 400, "message": str(er)}
            else:
                _request_ctx_stack.top.current_identity = identify
        return func(*args, **kwargs)

    return wrapper
