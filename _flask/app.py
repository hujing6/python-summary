# -*- coding: utf-8 -*-
__all__ = ["app",
           "api"]

from flask import Flask
from flasgger import Swagger
from flask_cors import CORS
from logger import config_logger
from resource.api import api

app = Flask(__name__)

api.init_app(app)

# 配置swagger
swagger_template = {
    "securityDefinitions": {"APIKeyHeader": {"type": "apiKey", "name": "Authorization", "in": "header"}}}
swagger = Swagger(app, template=swagger_template)
swagger.config.update({"title": "Test"})

# 配置 config_logger
config_logger()
# r'/*' 是通配符，让本服务器所有的 URL 都允许跨域请求
CORS(app, resources="/*")
